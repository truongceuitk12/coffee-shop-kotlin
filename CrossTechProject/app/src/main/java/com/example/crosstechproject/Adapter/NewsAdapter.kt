package com.example.crosstechproject.Adapter

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.crosstechproject.R
import com.example.crosstechproject.data.Models.News
import com.example.crosstechproject.ui.Fragments.Store.WebActivity
import com.squareup.picasso.Picasso

class NewsAdapter(val context: Context, var items: List<News>) :
    RecyclerView.Adapter<NewsAdapter.ViewHolderWeb>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderWeb {
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_news, parent, false)
        return ViewHolderWeb(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolderWeb, position: Int) {
        val p0 = items[position]
        holder.title.text = p0.title
        Picasso.get().load(p0.avatarUrl).into(holder.avatarUrl)
        holder.itemView.setOnClickListener {
            val intent = Intent(context, WebActivity::class.java)
            val bundle = Bundle()
            bundle.putString("_url", p0.webUrl)
            intent.putExtra("webUrl", bundle)
            context.startActivity(intent)

        }


    }


    fun updateData(list: List<News>) {
        items = list
        notifyDataSetChanged()
    }


    inner class ViewHolderWeb(view: View) : RecyclerView.ViewHolder(view) {
        val title = view.findViewById<TextView>(R.id.news_title)
        var avatarUrl = view.findViewById<ImageView>(R.id.news_img)
    }

}
