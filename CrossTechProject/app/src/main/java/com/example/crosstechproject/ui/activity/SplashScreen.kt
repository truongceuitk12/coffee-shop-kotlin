package com.example.crosstechproject.ui.activity


import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.example.crosstechproject.R
import com.example.crosstechproject.ui.Fragments.home.FragmentMain
import com.example.crosstechproject.ui.activity.LogIn.LogInActivity
import com.google.firebase.auth.FirebaseAuth

class SplashScreen : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        Handler().postDelayed({
            if (FirebaseAuth.getInstance().currentUser == null) {
                val intent = Intent(baseContext, LogInActivity::class.java)
                OverridePendingTransition()
                startActivity(intent)
            } else {
                val intent = Intent(baseContext, FragmentMain::class.java)
                intent.flags =
                    Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                OverridePendingTransition()
                finish()
            }
        }, 2000)
    }
}
