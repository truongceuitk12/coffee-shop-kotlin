package com.example.crosstechproject.utils

import android.content.Context
import android.net.ConnectivityManager
import android.util.Patterns
import java.util.regex.Pattern

fun isEmailValid(email: String): Boolean {
    return Patterns.EMAIL_ADDRESS.matcher(email).matches()
}

fun isPasswordValid(password: String): Boolean {
    val expression = "(?=.*[A-Z a-z])(?=.*[0-9]).{8,20}"
    val pattern = Pattern.compile(expression)
    val matcher = pattern.matcher(password)
    return matcher.matches()
}

fun Context.isConnectedToNetwork(): Boolean {
    val connectivityManager =
        this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
    return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting ?: false
}