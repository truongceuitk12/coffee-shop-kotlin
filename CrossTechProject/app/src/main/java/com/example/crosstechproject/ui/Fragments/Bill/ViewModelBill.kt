package com.example.crosstechproject.ui.Fragments.Bill

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.crosstechproject.data.Models.DrinkItem

class ViewModelBill : ViewModel() {
    val newBillItem = MutableLiveData<List<DrinkItem>>().apply { value = mutableListOf() }
}