package com.example.crosstechproject.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.crosstechproject.R
interface OnItemClickListener {
    fun onItemClick(item : String ,position: Int)
}

class KeywordAdapter(val context: Context, val list: List<String>, var  click : OnItemClickListener) :
    RecyclerView.Adapter<KeywordAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_keyword, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val hotKey: TextView = view.findViewById(R.id.keyword_text_word)
        fun initialize( item : String, action  : OnItemClickListener){
            hotKey.text = item
            itemView.setOnClickListener {
                action.onItemClick(item,adapterPosition)
            }
        }


    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.initialize(list.get(position), click)


    }


}