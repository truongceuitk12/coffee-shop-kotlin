package com.example.crosstechproject.ui.Fragments.Store

import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import com.example.crosstechproject.R
import com.example.crosstechproject.ui.activity.BaseActivity
import kotlinx.android.synthetic.main.activity_web_view.*

class WebActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
        val intent = getIntent()
        val bundle = intent.getBundleExtra("webUrl")
        val _url = bundle?.getString("_url")


        val webViewSetting = webView.settings
        webViewSetting.javaScriptEnabled = true
        webView.webViewClient = WebViewClient()
        webView.webChromeClient = WebChromeClient()
        webView!!.loadUrl(_url)
        webView.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                web_progress_bar.visibility = View.VISIBLE
                super.onPageStarted(view, url, favicon)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                web_progress_bar.visibility = View.GONE
            }
        }

    }

    override fun onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack()
        } else {
            super.onBackPressed()
        }


    }
}

