package com.example.crosstechproject.ui.Fragments.Ordel

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.crosstechproject.Adapter.MyPagerAdapter
import com.example.crosstechproject.R
import com.example.crosstechproject.ui.activity.ActivityLocation
import com.example.crosstechproject.ui.activity.ActivitySearch
import kotlinx.android.synthetic.main.fragment_dat_hang.*

class FragmentOrder : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_dat_hang, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showViewTabbar()
    }

    private fun showViewTabbar() {
        order_view_pager.adapter = MyPagerAdapter(childFragmentManager)
        order_tab_layout.setupWithViewPager(order_view_pager)
    }

}
