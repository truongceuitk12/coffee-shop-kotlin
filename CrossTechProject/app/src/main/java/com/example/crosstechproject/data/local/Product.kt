package com.example.crosstechproject.data.local

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "Product")
class Product(
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    val id: Int ,
    @ColumnInfo(name = "price")
    val price: Int,
    @ColumnInfo(name = "product_name")
    val name: String,
    @ColumnInfo(name = "amount")
    val amount: String,
    @ColumnInfo(name = "size")
    val size : String
)