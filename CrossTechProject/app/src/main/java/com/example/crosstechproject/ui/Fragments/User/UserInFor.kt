package com.example.crosstechproject.ui.Fragments.User

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.crosstechproject.R
import com.example.crosstechproject.data.Models.UserInfomation
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_tro_giup.*
import java.util.*

class UserInFor : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tro_giup)
        save_infor.setOnClickListener {
            putUser()
            Toast.makeText(this, "Lưu thành công", Toast.LENGTH_LONG).show()
            finish()
        }


    }

    private fun putUser() {
        val phone = intent.getStringExtra("phone")
        var name = edt_tennguoidung.text.toString()
        var address = edt_diachi.text.toString()
        var creditCardNumber = edt_creditnumber.text.toString()
        if (name.isEmpty() || address.isEmpty() || creditCardNumber.isEmpty()) {
            Toast.makeText(this, "Có chỗ còn trống", Toast.LENGTH_LONG).show()
        }
        val id = UUID.randomUUID().toString()
        val ref = FirebaseDatabase.getInstance().getReference("/userInfor/$id")
        val userInfor = UserInfomation(id, phone, name, address, creditCardNumber, "0", "0")
        ref.setValue(userInfor)
            .addOnSuccessListener {}
            .addOnFailureListener {}
    }
}
