package com.example.crosstechproject.data.Models


import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class News(
    val webUrl: String,
    val avatarUrl: String,
    val title: String
) : Parcelable {
    constructor() : this("", "", "")
}