package com.example.crosstechproject.data.Models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class UserInfomation(
    val id: String,
    val yourPhone: String,
    val name: String,
    val address: String,
    val creditNumber: String,
    val totalMoney: String,
    val nameOfItem: String
) : Parcelable {
    constructor() : this("", "", "", "", "", "", "")
}