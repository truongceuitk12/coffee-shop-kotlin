package com.example.crosstechproject.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Product::class],version = 1)
abstract class AppDB : RoomDatabase(){
        abstract fun drinkDAO() : DrinkDAO
    companion object {
        private var sInstance: AppDB? = null

        fun get(context: Context): AppDB? {
            if (sInstance == null) {
                synchronized(AppDB::class) {
                    sInstance = Room.databaseBuilder(context, AppDB::class.java, "db_drink")
                        .fallbackToDestructiveMigration()
                        .build()
                }
            }
            return sInstance
        }
    }

}