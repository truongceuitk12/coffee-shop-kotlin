package com.example.crosstechproject.ui.Fragments.Ordel

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.example.crosstechproject.Adapter.DrinkAdapter
import com.example.crosstechproject.R
import kotlinx.android.synthetic.main.fragment_ca_phe.*

class FragmentCoffee : Fragment() {

    val viewModel: ViewModelFragmentOder by lazy {
        ViewModelProviders
            .of(activity!!)
            .get(ViewModelFragmentOder::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_ca_phe, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.listCoffee.observe(this, Observer {
            fragment_coffee_rcv.apply {
                adapter = DrinkAdapter(activity!!, it)
                layoutManager = GridLayoutManager(activity, 2)
            }
        })

    }


}
