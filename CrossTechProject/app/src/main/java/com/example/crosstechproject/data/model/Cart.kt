package com.example.crosstechproject.data.model

class Cart {
    val id : Int = 0
    var qty : String = ""
    var name : String = ""
    var price : String = ""
    constructor(name: String, price: String, qty: String){
        this.name=name
        this.price=price
        this.qty=qty
    }
    constructor(){
    }
}