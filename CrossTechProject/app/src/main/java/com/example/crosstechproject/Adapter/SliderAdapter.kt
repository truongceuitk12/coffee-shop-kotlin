package com.example.crosstechproject.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.example.crosstechproject.R
import com.example.crosstechproject.data.Models.Slider
import com.smarteist.autoimageslider.SliderViewAdapter
import com.squareup.picasso.Picasso


class SliderAdapter(val context: Context, var list: List<Slider>) :
    SliderViewAdapter<SliderAdapter.ViewHolder>() {
    inner class ViewHolder(val view: View) : SliderViewAdapter.ViewHolder(view) {
        var image = view.findViewById<ImageView>(R.id.imgImageSlider)
    }

    override fun onCreateViewHolder(parent: ViewGroup?): ViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_slider_view, null)
        return ViewHolder(view)
    }

    override fun getCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder?, position: Int) {
        val p0 = list[position]
        Picasso.get().load(p0.imgUrl).fit().centerCrop()
            .into(viewHolder?.image)
    }

}

