package com.example.crosstechproject.data.Models


import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class DrinkItem(
    var avatarUrl: String,
    var name: String,
    var price: String,
    var id: String,
    var detail: String,
    var category: String,
    var isPopular: String
) : Parcelable {
    constructor() : this("", "", "", "", "", "", "")
}