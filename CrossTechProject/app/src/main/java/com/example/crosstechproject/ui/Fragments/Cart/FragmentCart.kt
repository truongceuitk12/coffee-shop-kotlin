package com.example.crosstechproject.ui.Fragments.Cart

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.room.Room
import com.example.crosstechproject.R
import com.example.crosstechproject.data.local.AppDB
import com.example.crosstechproject.data.local.Product
import com.example.crosstechproject.ui.Fragments.Ordel.FragmentOrder
import com.example.crosstechproject.ui.Fragments.home.FragmentMain
import kotlinx.android.synthetic.main.activity_fragment_main.*
import kotlinx.android.synthetic.main.fragment_gio_hang.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.support.v4.uiThread
import org.jetbrains.anko.uiThread

class FragmentCart : Fragment() {
    val viewModel: ViewModelCartFragment by lazy {
        ViewModelProviders
            .of(activity!!)
            .get(ViewModelCartFragment::class.java)
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_gio_hang, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cart_btn_order_now.setOnClickListener {
            openFragmentOrder()
        }

    }

    private fun openFragmentOrder() {
        activity!!.supportFragmentManager.beginTransaction()
            .replace(R.id.frmMain, FragmentOrder())
            .commit()
        (activity as FragmentMain).botNavigation.selectedItemId = R.id.nav_order
    }
}