package com.example.crosstechproject.data.Models


import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class BillItem(
    val id: String,
    val yourPhone: String,
    val name: String,
    val address: String,
    val totalMoney: String,
    val nameOfItem: String
) : Parcelable {
    constructor() : this("", "", "", "", "", "") {}
}
