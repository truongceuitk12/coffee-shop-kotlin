package com.example.crosstechproject.data.local

import androidx.lifecycle.LiveData
import com.example.crosstechproject.data.Models.Drink

class AppRepository() : CartDataSource {
    private val drinkDAO: DrinkDAO? = null
    private val mAllWords: LiveData<List<Product>>? = null

    override fun insertDrink(vararg product: Product) {
        return drinkDAO!!.insertDrink(*product)
    }

    override fun updateDrink(vararg product: Product) {
        TODO("Not yet implemented")
    }

    override fun deleteDrink(vararg Product: Product) {
        TODO("Not yet implemented")
    }

    override fun deleteDrink(id: String) {
        TODO("Not yet implemented")
    }

    override fun deleteAllDrink() {
        TODO("Not yet implemented")
    }



    override fun getAllDrink(): LiveData<List<Product>> {
        TODO("Not yet implemented")
    }


}