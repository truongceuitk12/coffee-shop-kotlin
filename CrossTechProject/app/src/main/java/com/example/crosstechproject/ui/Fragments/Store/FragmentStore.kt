package com.example.crosstechproject.ui.Fragments.Store

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.crosstechproject.Adapter.NewsAdapter
import com.example.crosstechproject.Adapter.SliderAdapter
import com.example.crosstechproject.R
import com.example.crosstechproject.data.Models.Slider
import com.example.crosstechproject.ui.Fragments.BaseFragment
import com.example.crosstechproject.ui.Fragments.Ordel.FragmentOrder
import com.example.crosstechproject.ui.Fragments.home.FragmentMain
import kotlinx.android.synthetic.main.activity_fragment_main.*
import kotlinx.android.synthetic.main.fragment_cua_hang.*


class FragmentStore : BaseFragment() {

    val viewModel: ViewModelFragmentStore by lazy {
        ViewModelProviders
            .of(activity!!)
            .get(ViewModelFragmentStore::class.java)
    }
    var listSlider = mutableListOf<Slider>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setFullScreen()
        return inflater.inflate(R.layout.fragment_cua_hang, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setFullScreen()
        autoSlider()
        showRecyclerView()
        store_btn_accumulate_point.setOnClickListener {
            putHandle()
        }
        store_btn_order.setOnClickListener {
            showFragmentOder()
        }
        store_btn_coupon.setOnClickListener {
            showCoupon()
        }
    }

    private fun showCoupon() {
        showMsg(getString(R.string.text_dont_have_coupon), "Thông báo")
    }

    private fun showFragmentOder() {

        activity!!.supportFragmentManager
            .beginTransaction()
            .replace(R.id.frmMain, FragmentOrder())
            .commit()
        (activity as FragmentMain).botNavigation.selectedItemId = R.id.nav_order
    }

    private fun putHandle() {
        val bundle = Bundle()
        val intent = Intent(activity!!, AcumulatePointActivity::class.java)
        viewModel.user.observe(this, Observer {
            if (it.uid == "") {
                showMsg(getString(R.string.text_please_try_again),"Lỗi hệ thống")
            } else {
                bundle.putString("_uid", it.uid)
                intent.putExtra("_uid", bundle)
                activity?.startActivity(intent)
            }

        })
    }

    private fun showRecyclerView() {
        store_rcv_news.run {
            viewModel.newWebItem.observe(this@FragmentStore, Observer {
                val adapter = NewsAdapter(activity!!, it)
                adapter.updateData(it)
                this.adapter = adapter
            })
        }
        store_rcv_coffee_lover.run {
            viewModel.newCoffeeLoveItem.observe(this@FragmentStore, Observer {
                val adapter = NewsAdapter(activity!!, it)
                adapter.updateData(it)
                this.adapter = adapter
            })
        }
        store_rcv_news_sale_news.run {
            viewModel.newNewsItem.observe(this@FragmentStore, Observer {
                val adapter = NewsAdapter(activity!!, it)
                adapter.updateData(it)
                this.adapter = adapter
            })
        }


    }


    private fun autoSlider() {
        listSlider.add(Slider("https://d9aim9fbtsqsm.cloudfront.net/wp-content/uploads/2019/02/thedome01.jpg"))
        listSlider.add(Slider("https://media-cdn.tripadvisor.com/media/photo-s/10/fd/34/d4/beautiful-coffee-shop.jpg"))
        listSlider.add(Slider("https://s.yimg.com/uu/api/res/1.2/IMvI3jOYhsyYPrJ.qY_Dog--~B/aD0zMDAwO3c9NDUwMDtzbT0xO2FwcGlkPXl0YWNoeW9u/http://media.zenfs.com/en-US/homerun/architectural_digest_422/dfcece9b6b3b40eacb861e8c7d2340f3"))
        store_slider.run {
            sliderAdapter = SliderAdapter(activity!!, listSlider)
            scrollTimeInSec = 4
            startAutoCycle()
        }
    }





}


