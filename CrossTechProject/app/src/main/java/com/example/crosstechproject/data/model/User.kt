package com.example.crosstechproject.data.Models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class User(
    val yourPhone: String,
    val uid: String,
    val profileImageUrl: String,
    val accumulatePoints: String,
    val timesOfOrder: String
) : Parcelable {
    constructor() : this("", "", "", "", "")
}