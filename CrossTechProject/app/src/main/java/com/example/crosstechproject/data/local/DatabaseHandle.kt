package com.example.crosstechproject.data.local

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast
import com.example.crosstechproject.data.model.Cart

const val DB_NAME = "db_project"
const val TABLE_NAME = "cart"
const val COL_NAME = "name"
const val COL_PRICE = "price"
const val COL_QTY = "qty"
const val COL_ID = "id"

class DatabaseHandle(var context: Context) : SQLiteOpenHelper(context, DB_NAME, null, 1) {
    override fun onCreate(p0: SQLiteDatabase?) {
        val createTable = "CREATE TABLE " + TABLE_NAME + "(" +
                COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COL_NAME +
                "VARCHAR(256)," + COL_PRICE + "VARCHAR(256)," + COL_QTY + "VARCHAR(256))"
        p0?.execSQL(createTable)
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        TODO("Not yet implemented")
    }

    fun insertData(cart: Cart) {
        val db = this.writableDatabase
        var cv = ContentValues()
        cv.put(COL_NAME, cart.name)
        cv.put(COL_PRICE, cart.price)
        cv.put(COL_QTY, cart.qty)
        var result = db.insert(TABLE_NAME, null, cv)
        if (result == -1.toLong())
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        else
            Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show()


    }

}

