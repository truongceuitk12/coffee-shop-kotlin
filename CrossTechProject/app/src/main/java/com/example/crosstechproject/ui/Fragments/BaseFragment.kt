package com.example.crosstechproject.ui.Fragments

import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.example.crosstechproject.R

abstract class BaseFragment : Fragment() {
    fun showMsg(msg: String, notify : String?) {
        val builder = AlertDialog.Builder(activity!!)
        builder.setMessage(msg)
            .setTitle(notify)
            .setCancelable(false)
            .setPositiveButton("Yes") { _, _ ->
            }
        builder.show()
    }
     fun setFullScreen() {
        getActivity()!!.getWindow().setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }



}