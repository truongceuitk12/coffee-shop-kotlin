package com.example.crosstechproject.data.Models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Slider(var imgUrl: String) : Parcelable {
    constructor() : this("")
}