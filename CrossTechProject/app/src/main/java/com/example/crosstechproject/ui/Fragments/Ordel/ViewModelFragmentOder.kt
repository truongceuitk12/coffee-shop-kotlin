package com.example.crosstechproject.ui.Fragments.Ordel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.crosstechproject.data.Models.Drink
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class ViewModelFragmentOder : ViewModel() {
    val item = mutableListOf<Drink>()
    val popular = mutableListOf<Drink>()
    val tea = mutableListOf<Drink>()
    val coffee = mutableListOf<Drink>()
    val vitamin = mutableListOf<Drink>()
    val list = MutableLiveData<List<Drink>>().apply { value = mutableListOf() }
    val listPopular = MutableLiveData<List<Drink>>().apply { value = mutableListOf() }
    val listCoffee = MutableLiveData<List<Drink>>().apply { value = mutableListOf() }
    val listTea = MutableLiveData<List<Drink>>().apply { value = mutableListOf() }
    val listVitamin = MutableLiveData<List<Drink>>().apply { value = mutableListOf() }

    init {
        fetchProduct()
    }

    private fun fetchProduct() {
        val ref = FirebaseDatabase.getInstance().getReference("/product")
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {}
            override fun onDataChange(p0: DataSnapshot) {
                p0.children.forEach() {
                    val product = it.getValue(Drink::class.java)

                    if (product != null) {

                        popular.add(product)

                    }
                    when (product?.category) {
                        "1" -> {
                            coffee.add(product)
                        }
                        "3" -> {
                            tea.add(product)
                        }
                        "2" -> {
                            vitamin.add(product)
                        }
                    }
                }
                listPopular.value = popular
                listCoffee.value = coffee
                listTea.value = tea
                listVitamin.value = vitamin

            }

        })
    }


}