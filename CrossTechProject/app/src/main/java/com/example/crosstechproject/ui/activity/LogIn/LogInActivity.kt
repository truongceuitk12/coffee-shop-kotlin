package com.example.crosstechproject.ui.activity.LogIn

import android.content.Intent
import android.os.Bundle
import com.example.crosstechproject.R
import com.example.crosstechproject.ui.Fragments.home.FragmentMain
import com.example.crosstechproject.ui.activity.BaseActivity
import com.example.crosstechproject.utils.isConnectedToNetwork
import com.example.crosstechproject.utils.isEmailValid
import com.example.crosstechproject.utils.isPasswordValid
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*


class LogInActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        layout_login_activity.setOnTouchListener { v, event ->
            hideSoftKeyBoard()
            true
        }

        login_btn_forgot_password.setOnClickListener {
            val intent = Intent(this, ForgotPass::class.java)
            startActivity(intent)
            OverridePendingTransition()
        }

        login_btn_register_account.setOnClickListener {


            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
            OverridePendingTransition()


        }

        login_btn_sign_in.setOnClickListener {
            if (this.isConnectedToNetwork() == false) {
                showMsgFragment(getString(R.string.text_check_your_network))
            } else {
                performLogin()
            }

        }


    }

    private fun performLogin() {
        val email = youremail_edt_login.text.trim().toString()
        val password = password_edt_login.text.trim().toString()
        if (email.isEmpty() || password.isEmpty()) {
            showMsgFragment(getString(R.string.all_text_empty_field))
            return
        } else if (!isEmailValid(email) || !isPasswordValid(password)) {
            showMsgFragment(getString(R.string.all_text_incorrect_input))
            return
        } else {
            showLoading()
        }
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if (!it.isSuccessful) return@addOnCompleteListener
                val intent = Intent(this, FragmentMain::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                OverridePendingTransition()

            }
            .addOnFailureListener {
            }
    }


}