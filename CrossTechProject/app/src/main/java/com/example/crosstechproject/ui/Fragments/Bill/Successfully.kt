package com.example.crosstechproject.ui.Fragments.Bill

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.crosstechproject.ui.Fragments.home.FragmentMain
import com.example.crosstechproject.R
import kotlinx.android.synthetic.main.activity_successfully.*

class Successfully : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_successfully)
        continue_to_buy_btn.setOnClickListener {
            val intent = Intent(this, FragmentMain ::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
    }
}
