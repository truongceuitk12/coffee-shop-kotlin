package com.example.crosstechproject.ui.Fragments.Bill

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.crosstechproject.data.Models.DrinkItem
import com.example.crosstechproject.data.Models.BillItem
import com.example.crosstechproject.data.Models.UserInfomation
import com.example.crosstechproject.R
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_customer_information.*
import java.util.*

class CustomerInformation : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customer_information)
        fetchUserInfor()
        btn_send_bill.setOnClickListener {
            pushThisBill()
            val intent = Intent(this, Successfully ::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }



    }

    private fun fetchUserInfor() {
        val ref = FirebaseDatabase.getInstance().getReference("/userInfor")
        ref.addChildEventListener(object : ChildEventListener {
            override fun onCancelled(p0: DatabaseError) {}
            override fun onChildMoved(p0: DataSnapshot, p1: String?) {}
            override fun onChildChanged(p0: DataSnapshot, p1: String?) {}
            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                val user = p0.getValue(UserInfomation::class.java)
                if (user != null) {
                    tv_ten_cfbill.text = user.name
                    tv_sodt_cfbill.text = user.yourPhone
                    tv_creditnumber_cfbill.text = user.creditNumber
                    tv_diachi_cfbill.text = user.address
                }

                val item = intent.getParcelableExtra<DrinkItem>("information")
                tv_tong_tien_cfbill.text=item!!.price
                tv_ten_san_pham_cfbill.text=item.name
            }

            override fun onChildRemoved(p0: DataSnapshot) {}
        })

    }

    private fun pushThisBill() {
        val totalMoney = tv_tong_tien_cfbill.text.toString()
        val name = tv_ten_san_pham_cfbill.text.toString()
        val address = tv_diachi_cfbill.text.toString()
        val creditCardNumber = tv_creditnumber_cfbill.text.toString()
        val phone = tv_sodt_cfbill.text.toString()
        val id = UUID.randomUUID().toString()
        val ref = FirebaseDatabase.getInstance().getReference("/Bills/$id")
        val userInfor = BillItem(id,phone,address,totalMoney,creditCardNumber,name)
        ref.setValue(userInfor)
                .addOnSuccessListener {}
                .addOnFailureListener {}
    }



}
