package com.example.crosstechproject.data.Models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Drink(
    val avatarUrl: String,
    val name: String,
    val price: String,
    var id: String,
    val detail: String,
    val category: String,
    val popular: String
) : Parcelable {
    constructor() : this("", "", "", "", "", "", "")
}