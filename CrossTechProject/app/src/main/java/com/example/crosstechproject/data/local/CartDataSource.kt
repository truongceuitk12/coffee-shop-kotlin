package com.example.crosstechproject.data.local

import androidx.lifecycle.LiveData

interface CartDataSource {

    fun insertDrink(vararg product: Product)

    fun updateDrink(vararg product : Product)

    fun deleteDrink(vararg Product : Product)

    fun deleteAllDrink()

    fun deleteDrink(id : String )


    fun getAllDrink():LiveData<List<Product>>
}