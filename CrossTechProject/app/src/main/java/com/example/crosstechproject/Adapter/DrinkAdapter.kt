package com.example.crosstechproject.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.example.crosstechproject.R
import com.example.crosstechproject.data.Models.Drink
import com.example.crosstechproject.ui.Fragments.Cart.ProductDetails
import com.squareup.picasso.Picasso
import java.text.DecimalFormat


class DrinkAdapter(val context: Context, var list: List<Drink>) :
    RecyclerView.Adapter<DrinkAdapter.ViewHolder>() {
    val fragment: Fragment? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_drink, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val p0 = list[position]
        val price = p0.price.toInt()
        holder.name.text = p0.name
        holder.price.text = formatNumber(price) + " đ"
        Picasso.get().load(p0.avatarUrl).into(holder.avatarUrl)
        holder.itemView.setOnClickListener {
            val intent = Intent(context, ProductDetails::class.java)
            val bundle = Bundle()
            bundle.apply {
                putString("_name" , p0.name)
                putString("_price" , p0.price)
                putString("_detail" , p0.detail)
                putString("_url" , p0.avatarUrl)
            }
            intent.putExtra("details", bundle)

            context.startActivity(intent)

        }


    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var name = view.findViewById<TextView>(R.id.drink_text_name)
        var price = view.findViewById<TextView>(R.id.drink_text_price)
        var avatarUrl = view.findViewById<ImageView>(R.id.drink_img_avatar)

    }

    private fun formatNumber(number: Int): String {
        val currency = DecimalFormat("###,###")
        return currency.format(number)
    }


}


