package com.example.crosstechproject.ui.Fragments.home

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.crosstechproject.R
import kotlinx.android.synthetic.main.activity_fragment_main.*


class FragmentMain : AppCompatActivity() {
    var idMenuSelected: Int = R.id.nav_store
    val fragmentStore by lazy {
        com.example.crosstechproject.ui.Fragments.Store.FragmentStore()
    }
    val fragmentOrder by lazy {
        com.example.crosstechproject.ui.Fragments.Ordel.FragmentOrder()
    }
    val fragmentCart by lazy {
        com.example.crosstechproject.ui.Fragments.Cart.FragmentCart()
    }
    val fragmentNotify by lazy {
        com.example.crosstechproject.ui.Fragments.Notify.FragmentNotify()
    }
    val fragmentUser by lazy {
        com.example.crosstechproject.ui.Fragments.User.FragmentUser()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment_main)
        ViewModelProviders.of(this)
            .get(ViewModelHome::class.java)

        supportFragmentManager
            .beginTransaction()
            .add(R.id.frmMain, com.example.crosstechproject.ui.Fragments.Store.FragmentStore())
            .commit()

        botNavigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.nav_store -> {
                    if (idMenuSelected != R.id.nav_store) {
                        idMenuSelected = R.id.nav_store
                        showFragment(fragmentStore)

                    }
                    true
                }
                R.id.nav_order -> {
                    if (idMenuSelected != R.id.nav_order) {
                        idMenuSelected = R.id.nav_order
                        showFragment(fragmentOrder)
                    }
                    true

                }
                R.id.nav_cart -> {
                    if (idMenuSelected != R.id.nav_cart) {
                        idMenuSelected = R.id.nav_cart
                        showFragment(fragmentCart)
                    }
                    true

                }
                R.id.nav_notify -> {
                    if (idMenuSelected != R.id.nav_notify) {
                        idMenuSelected = R.id.nav_notify
                        showFragment(fragmentNotify)

                    }
                    true

                }
                R.id.nav_account -> {
                    if (idMenuSelected != R.id.nav_account) {
                        idMenuSelected = R.id.nav_account
                        showFragment(fragmentUser)
                    }
                    true
                }
                else -> false
            }
        }
    }

    private fun showFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.frmMain, fragment)
            .commit()
    }


}


