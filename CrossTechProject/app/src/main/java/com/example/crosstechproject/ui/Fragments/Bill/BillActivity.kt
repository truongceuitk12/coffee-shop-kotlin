package com.example.crosstechproject.ui.Fragments.Bill

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.example.crosstechproject.Adapter.BillItemAdapter
import com.example.crosstechproject.ui.Fragments.home.FragmentMain
import com.example.crosstechproject.data.Models.DrinkItem
import com.example.crosstechproject.R
import kotlinx.android.synthetic.main.activity_bill.*

class BillActivity : AppCompatActivity() {
    var listBill = mutableListOf<DrinkItem>()
    var totalMoney = 0
    val viewModel: ViewModelBill by lazy {
        ViewModelProviders
                .of(this!!)
                .get(ViewModelBill::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bill)
        val item = intent.getParcelableExtra<DrinkItem>("bill_item")
        listBill.add(item)
        val adapter = BillItemAdapter(baseContext, listBill)
        adapter.notifyDataSetChanged()
        recycler_view_bill.setAdapter(adapter)

        back_to_cart_btn_bill.setOnClickListener {
            val intent = Intent(this, FragmentMain::class.java)
            startActivity(intent)
        }
        delete_bill_btn_bill.setOnClickListener {
            val intent = Intent(this, FragmentMain::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)

        }
        //hien thi
        listBill.forEach {
            totalMoney += it.price.toInt()
        }
        total_money_tv_bill.text = totalMoney.toString() + "Đ"
        pay_money_btn_bill.setOnClickListener {
            val intent = Intent(this, CustomerInformation::class.java)
           // intent.putExtra("totalMoney", totalMoney)
            intent.putExtra("information", item)
            startActivity(intent)
        }

    }
}
