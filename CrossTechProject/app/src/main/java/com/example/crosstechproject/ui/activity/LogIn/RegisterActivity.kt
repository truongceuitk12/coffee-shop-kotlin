package com.example.crosstechproject.ui.activity.LogIn

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import com.example.crosstechproject.R
import com.example.crosstechproject.data.Models.User
import com.example.crosstechproject.ui.Fragments.home.FragmentMain
import com.example.crosstechproject.ui.activity.BaseActivity
import com.example.crosstechproject.utils.isConnectedToNetwork
import com.example.crosstechproject.utils.isEmailValid
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class RegisterActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        register_btn_back.setOnClickListener {
            onBackPressed()
        }
        register_btn_forgotpass.setOnClickListener {
            startActivity(Intent(this,ForgotPass::class.java))
        }
        frm_register.setOnTouchListener { _, _ ->
            hideSoftKeyBoard()
            true
        }
        register_btn_register.setOnClickListener {
            if (this.isConnectedToNetwork() == false) {
                showMsgFragment(getString(R.string.text_check_your_network))
            } else {
                performRegister()
            }

        }
        select_image_view_btn_register.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 0)

        }
        allready_have_an_account_tv.setOnClickListener {
            val intent = Intent(this, LogInActivity::class.java)
            startActivity(intent)
        }
    }

    var selectPhotoUri: Uri? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            selectPhotoUri = data.data
            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, selectPhotoUri)
            select_image_view_btn_register.setImageBitmap(bitmap)

        }
    }

    // create account with firebase
    private fun performRegister() {
        val confirmPassword = confirmPassword_edt_register.text.trim().toString()
        val email = youremail_edt_register.text.trim().toString()
        val password = password_edt_register.text.trim().toString()
        when {
            email.isEmpty() || password.isEmpty() -> {
                showMsgFragment(getString(R.string.all_text_empty_field))
                return
            }
            password != confirmPassword -> {
                showMsgFragment(getString(R.string.all_text_incorrect_confirm_pass))
                return
            }
            !isEmailValid(email) -> {
                showMsgFragment(getString(R.string.all_text_incorrect_email))
                return
            }
            !isEmailValid(email) -> {
                showMsgFragment(getString(R.string.all_text_incorrect_password))
                return
            }
            else -> {
                showLoading()
            }
        }
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if (!it.isSuccessful) return@addOnCompleteListener
                uploadImageToFirebaseStorage()
            }
            .addOnFailureListener {
                showMsgFragment(getString(R.string.all_checking_network))
            }
    }

    private fun uploadImageToFirebaseStorage() {
        if (selectPhotoUri == null) {
            showMsgFragment(getString(R.string.all_text_select_photo))
            return
        }
        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/Images/$filename")
        ref.putFile(selectPhotoUri!!)
            .addOnSuccessListener {
                Log.d("RegisterActivity", "Succesfully upload image : ${it.metadata?.path}")
                ref.downloadUrl.addOnSuccessListener {
                    Log.d("RegisterActivity", "File location: $it")
                    saveUserToFirebaseDatabase(it.toString())
                }
            }
            .addOnFailureListener {
            }
    }

    private fun saveUserToFirebaseDatabase(profileImageUrl: String) {
        val uid = FirebaseAuth.getInstance().uid ?: ""
        val ref = FirebaseDatabase.getInstance().getReference("/user/$uid")
        val user = User(yourphone_edt_register.text.toString(), uid, profileImageUrl, "0", "0")
        ref.setValue(user)
            .addOnSuccessListener {
                Log.d("RegisterActivity", "Finally")
                val intent = Intent(this, FragmentMain::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                OverridePendingTransition()
            }
            .addOnFailureListener {
                Log.d("CheckErr", "Failed to set value to database: ${it.message}")
            }

    }


}



