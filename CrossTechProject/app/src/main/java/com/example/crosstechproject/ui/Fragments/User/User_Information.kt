package com.example.crosstechproject.ui.Fragments.User

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.crosstechproject.R
import com.example.crosstechproject.data.Models.UserInfomation
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_user__information.*

class User_Information : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user__information)
        fetchAndShowData()
    }

    private fun fetchAndShowData() {
        val ref = FirebaseDatabase.getInstance().getReference("/userInfor")
        ref.addChildEventListener(object : ChildEventListener {
            override fun onCancelled(p0: DatabaseError) {}
            override fun onChildMoved(p0: DataSnapshot, p1: String?) {}
            override fun onChildChanged(p0: DataSnapshot, p1: String?) {}
            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                val user = p0.getValue(UserInfomation::class.java)
                if (user != null) {
                    tv_ten.text = user.name
                    tv_sodt.text = user.yourPhone
                    tv_creditnumber.text = user.creditNumber
                    tv_diachi.text = user.address
                }
                Log.d("####", "Conmemememee")
                val email = intent.getStringExtra("email")
                tv_email.text = email
            }

            override fun onChildRemoved(p0: DataSnapshot) {}
        })


    }
}
