package com.example.crosstechproject.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.crosstechproject.R
import com.example.crosstechproject.data.Models.DrinkItem
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView


class BillItemAdapter(val context: Context, var list: List<DrinkItem>) :
    RecyclerView.Adapter<BillItemAdapter.BillViewHoler>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BillViewHoler {
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_bill, parent, false)
        return BillViewHoler(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: BillViewHoler, position: Int) {
        val p0 = list[position]
        holder.name.text = p0.name
        holder.price.text = p0.price + "đ"
        Picasso.get().load(p0.avatarUrl).into(holder.avatarUrl)
    }

    inner class BillViewHoler(view: View) : RecyclerView.ViewHolder(view) {
        var name = view.findViewById<TextView>(R.id.name_tv_item_bill)
        var price = view.findViewById<TextView>(R.id.price_tv_item_bill)
        var total = view.findViewById<TextView>(R.id.total_item_tv_item_bill)
        var avatarUrl = view.findViewById<CircleImageView>(R.id.avatar_img_item_bill)

    }

    fun updateDataSetChange(billsItems: List<DrinkItem>?) {
        billsItems?.let {
            list = billsItems
            notifyDataSetChanged()
        }
    }
}
