package com.example.crosstechproject.data.local

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface DrinkDAO{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDrink(vararg product: Product)
    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateDrink(vararg product : Product)
    @Delete
    fun deleteDrink(product : Product)
    @Query("select *  from Product")
    fun getAllDrink() : LiveData<List<Product>>
    @Query("delete from Product")
    fun deleteAllDrink()
    @Query("delete from Product where product_name = :id")
    fun deleteDrink(id : String )

}