package com.example.crosstechproject.ui.Fragments.Store

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.crosstechproject.data.Models.News
import com.example.crosstechproject.data.Models.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class ViewModelFragmentStore : ViewModel() {


    var user = MutableLiveData<User>().apply { value = User() }
    val newWebItem = MutableLiveData<List<News>>().apply { value = mutableListOf() }
    val newNewsItem = MutableLiveData<List<News>>().apply { value = mutableListOf() }
    val newCoffeeLoveItem = MutableLiveData<List<News>>().apply { value = mutableListOf() }
    var itemNews = mutableListOf<News>()
    var itemCoffeeLover = mutableListOf<News>()
    val webItem = mutableListOf<News>()

    init {
        if (newCoffeeLoveItem.value.isNullOrEmpty()) {
            fetchItemNews()
            fetchSaleNewsItem()
            fetchCoffeeLoveItem()
            fetchUser()
        }

    }

    fun fetchItemNews() {

        val ref = FirebaseDatabase.getInstance().getReference("/web")

        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
            }

            override fun onDataChange(p0: DataSnapshot) {
                p0.children.forEach {
                    Log.d("New item", it.toString())
                    val web = it.getValue(News::class.java)
                    if (web != null) {
                        webItem.add(web)
                    }
                }
                newWebItem.value = webItem
            }
        })
    }

    fun fetchUser() {
        val uid = FirebaseAuth.getInstance().uid
        FirebaseDatabase.getInstance().reference.child("/user").child(uid!!)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                }

                override fun onDataChange(p0: DataSnapshot) {
                    val data = p0.getValue(User::class.java)
                    user.value = data
                }

            })

    }

    fun fetchCoffeeLoveItem() {
        val ref = FirebaseDatabase.getInstance().getReference("/coffee_lover")

        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
            }

            override fun onDataChange(p0: DataSnapshot) {
                p0.children.forEach {
                    val web = it.getValue(News::class.java)
                    if (web != null) {
                        itemCoffeeLover.add(web)
                    }
                }
                newCoffeeLoveItem.value = itemCoffeeLover
            }
        })

    }

    fun fetchSaleNewsItem() {
        val ref = FirebaseDatabase.getInstance().getReference("/news")

        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
            }

            override fun onDataChange(p0: DataSnapshot) {
                p0.children.forEach {
                    val web = it.getValue(News::class.java)
                    if (web != null) {
                        itemNews.add(web)
                    }
                }
                newNewsItem.value = itemNews
            }
        })

    }

}