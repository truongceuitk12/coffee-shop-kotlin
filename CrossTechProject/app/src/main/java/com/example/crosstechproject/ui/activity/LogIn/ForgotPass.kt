package com.example.crosstechproject.ui.activity.LogIn

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.example.crosstechproject.R
import com.example.crosstechproject.ui.activity.BaseActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_forgot.*

class ForgotPass : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot)
        forgot_btn_reset_pass.setOnClickListener {
            sendAPassWordReset()
            forgot_btn_back.setOnClickListener {
                onBackPressed()
            }
        }
    }

    private fun sendAPassWordReset() {
        val auth: FirebaseAuth = FirebaseAuth.getInstance()
        val email = forgot_edt_email.text?.trim().toString()
        auth.sendPasswordResetEmail(email)
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    val builder = AlertDialog.Builder(this)
                    builder.setMessage("Email mật khẩu đã được gửi vào email của bạn !")

                        .setCancelable(false)
                        .setPositiveButton("Yes") { _, _ ->
                            startActivity(Intent(this, LogInActivity::class.java))
                            OverridePendingTransition()
                        }
                    builder.show()

                } else {
                    showMsgFragment("Please check your email !")
                }
            }
    }

}
