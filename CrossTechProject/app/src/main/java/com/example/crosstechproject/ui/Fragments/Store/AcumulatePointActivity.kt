package com.example.crosstechproject.ui.Fragments.Store

import android.os.Bundle
import com.example.crosstechproject.R
import com.example.crosstechproject.ui.activity.BaseActivity
import kotlinx.android.synthetic.main.activity_acumulate_point.*

class AcumulatePointActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_acumulate_point)
        val intent = getIntent()
        val bundle = intent.getBundleExtra("_uid")
        val _uid = bundle?.getString("_uid")
        accumulate_point_uid.text = _uid
        accumulate_btn_back.setOnClickListener {
            onBackPressed()
        }
    }
}
