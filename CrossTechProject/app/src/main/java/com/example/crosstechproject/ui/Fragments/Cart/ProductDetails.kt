package com.example.crosstechproject.ui.Fragments.Cart

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.crosstechproject.R
import com.example.crosstechproject.data.local.DatabaseHandle
import com.example.crosstechproject.data.model.Cart
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_product_details.*

class ProductDetails : AppCompatActivity() {
    private var _name: String? = null
    private var _price: String? = null
    private var _detail: String? = null
    private var _avatar: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_details)
        val context = this
        var db = DatabaseHandle(context)
        details_btn_back.setOnClickListener {
            onBackPressed()
        }
        getData()
        val imgProduct = findViewById<CircleImageView>(R.id.details_img_product)
        Picasso.get().load(_avatar).into(imgProduct)
        details_text_name.text = _name

        details_text_content.text = _detail
        var temp = 1
        details_lo_sub.setOnClickListener {
            if (temp > 1) temp--
            details_text_qty.text = temp.toString()
        }
        details_lo_plus.setOnClickListener {
            temp++
            details_text_qty.text = temp.toString()
        }

        details_lo_add_cart.setOnClickListener {
            if (_name != null && _price != null) {
                val cart = Cart(_name!!, _price!!, temp.toString())
                db.insertData(cart)
            }

        }


    }

    private fun getData() {
        val intent = intent
        val bundle = intent.getBundleExtra("details")
        _name = bundle?.getString("_name")
        _price = bundle?.getString("_price")
        _detail = bundle?.getString("_detail")
        _avatar = bundle?.getString("_url")
    }

}
