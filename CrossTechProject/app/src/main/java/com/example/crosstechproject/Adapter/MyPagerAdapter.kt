package com.example.crosstechproject.Adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.crosstechproject.ui.Fragments.Ordel.FragmentCoffee
import com.example.crosstechproject.ui.Fragments.Ordel.FragmentPopularDrink
import com.example.crosstechproject.ui.Fragments.Ordel.FragmentTea
import com.example.crosstechproject.ui.Fragments.Ordel.FragmentVitamin

class MyPagerAdapter(frm: FragmentManager) : FragmentPagerAdapter(frm) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                FragmentPopularDrink()
            }
            2 -> {
                FragmentCoffee()
            }
            1 -> {
                FragmentTea()
            }
            else -> {
                FragmentVitamin()
            }

        }
    }

    override fun getCount(): Int {
        return 4
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Phổ biến"
            2 -> "Cà phê"
            1 -> "Trà"
            else -> {
                return "Sinh tố"
            }
        }
    }
}