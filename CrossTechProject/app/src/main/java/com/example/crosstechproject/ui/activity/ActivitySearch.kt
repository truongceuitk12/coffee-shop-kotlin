package com.example.crosstechproject.ui.activity


import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.example.crosstechproject.Adapter.KeywordAdapter
import com.example.crosstechproject.Adapter.OnItemClickListener
import com.example.crosstechproject.R
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_search.*


class ActivitySearch : BaseActivity(), OnItemClickListener {
    val ref = FirebaseDatabase.getInstance().getReference("/product")


    val list = listOf("Cà phê sữa", "Capuchino", "Late", "Bạc xỉu", "Trà ô long", " Sinh tố")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        search_btn_back.setOnClickListener {
            onBackPressed()
        }
        search_rcv_list_key.run {
            adapter = KeywordAdapter(context, list, this@ActivitySearch)
            layoutManager = GridLayoutManager(context, 3)
        }


    }

    override fun onItemClick(item: String, position: Int) {
        Toast.makeText(this, list[position], Toast.LENGTH_LONG).show()
        (search_field as TextView).text = list[position]
        scroll_view_hotkey.visibility = View.INVISIBLE
        search_text_hotkey.visibility = View.INVISIBLE
    }


}
