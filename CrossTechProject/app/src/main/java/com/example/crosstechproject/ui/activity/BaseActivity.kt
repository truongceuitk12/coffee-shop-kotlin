package com.example.crosstechproject.ui.activity

import android.app.ProgressDialog
import android.content.Context
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.crosstechproject.R

abstract class BaseActivity : AppCompatActivity() {

    fun OverridePendingTransition() {
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    fun showMsgFragment(msg: String) {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(msg)
            .setTitle(getString(R.string.all_text_error))
            .setCancelable(false)
            .setPositiveButton("Yes") { _, _ ->
            }
        builder.show()
    }

    fun hideSoftKeyBoard() {
        currentFocus?.let {
            val inputMethodManager: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(it.windowToken, 0)
        }
    }

    fun showLoading() {
        val progressDialog = ProgressDialog(this)
        progressDialog.let {
            it.show()
            it.setContentView(R.layout.dialog_loading)
            it.window?.setBackgroundDrawableResource(android.R.color.transparent)
        }
        progressDialog.setCancelable(false)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

}